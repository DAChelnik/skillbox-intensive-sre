<?php 

# надо будет потом взять данные с YAML-настроек
define( 'DB_NAME', 'skillbox_wp' );
define( 'DB_USER', 'root' );
define( 'DB_PASSWORD', 'p@ssw0rd' );

define( 'WP_DEBUG', false );

$table_prefix = 'wp_';

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', __DIR__ . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once ABSPATH . 'wp-settings.php';
