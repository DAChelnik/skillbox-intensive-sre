# подключаем и читаем YAML-файл с настройками
require 'yaml'
current_dir = File.join(File.dirname(__FILE__))
if File.file?('./config/provision/vagrant-config.yaml')
  vgconfig = YAML.load_file("#{current_dir}/config/provision/vagrant-config.yaml")
else
  raise "Конфигурационный файл отсутствует"
end

Vagrant.configure("2") do |config|
    
  config.vm.define vgconfig["NAME"] do |web|
    web.trigger.before :halt do |trigger|
      trigger.name = "VAGRANT HALT #{vgconfig["NAME"]}"
      trigger.info = "\e[34mСоздаём бэкап базы данных перед завершение работы\e[0m"
      trigger.run_remote  = {path: "#{current_dir}/config/provision/vagrant-halt.sh"}
    end
    web.vm.box = vgconfig["BOX_IMAGE"]
    web.vm.box_check_update = vgconfig["CHECK_UPDATE"]
    #
    # РАСШИРЕННАЯ КОНФИГУРАЦИЯ:
    web.vm.provider "virtualbox" do |vb|
      vb.name = vgconfig["NAME"]
      vb.memory = vgconfig["MEMORY"]
      vb.cpus = vgconfig["CPU"]
    end
    #
    # ПРОБРОС ПОРТОВ:
    # следующая настройка позволит нам открыть порт прослушивания в хост- и гостевой операционных системах. 
    # Хост- операционная система пересылает все полученные пакеты на порт, который мы указываем для гостевой операционной системы.
    # Для MySQL стандартный порт 3306, значения берутся из YAML-файла с настройками. 
    # Для веб-приложений, например для phpmyadmin, значения берутся из YAML-файла с настройками.
    # Если у вас уже используется этот порт, вы можете изменить его.
    web.vm.network "forwarded_port", guest: vgconfig["MYSQL_PORT"]["GUEST"], host: vgconfig["MYSQL_PORT"]["HOST"]
    web.vm.network "forwarded_port", guest: vgconfig["PORT2"]["GUEST"], host: vgconfig["PORT2"]["HOST"]
    #  
    # СИНХРОНИЗАЦИЯ ФАЙЛОВ ПРОЕКТА:
    # Хорошей практикой является не копирование файлов проекта в виртуальную машину, а совместное использование файлов между хостом и 
    # гостевыми операционными системами, потому что если вы удалите свою виртуальную машину, файлы будут потеряны с ней.
    # Первым аргументом является папка на хост-машине, которая будет использоваться совместно с виртуальной машиной.
    # Второй аргумент – это целевая папка внутри виртуальной машины.
    # create: true указывает, что если целевой папки внутри виртуальной машины не существует, то необходимо создать ее автоматически.
    # group: «www-data» и owner: «www-data» указывает владельца и группу общей папки внутри виртуальной машины. 
    # По умолчанию большинство веб-серверов используют www-данные в качестве владельца, обращающегося к файлам.
    # web.vm.synced_folder ".", "/vagrant", disabled: true
    web.vm.synced_folder "./www", "/var/www/html", disabled: true
    web.vm.synced_folder vgconfig["VHOSTS_DIR"], "/var/tmp/skillbox-vhosts", create: true
    web.vm.synced_folder vgconfig["DOCUMENT_ROOT"], vgconfig["APACHE_DOCUMENT_ROOT"], create: true, owner: "www-data", group: "www-data"
    #
  end
    
  config.vm.provision "shell", path: "#{current_dir}/config/provision/provision.sh", 
    env: {
      "PHPVERSION"  => "#{vgconfig["PHP_VERSION"]}", 
      "DBHOST"      => "#{vgconfig["MYSQL_DBHOST"]}", 
      "WPDBNAME"    => "#{vgconfig["MYSQL_WP_DBNAME"]}", 
      "DPDBNAME"    => "#{vgconfig["MYSQL_DP_DBNAME"]}", 
      "DBUSER"      => "#{vgconfig["MYSQL_DBUSER"]}", 
      "DBPASSWD"    => "#{vgconfig["MYSQL_DBPASSWD"]}"
    }
end
