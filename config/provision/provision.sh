#!/bin/bash
export DEBIAN_FRONTEND=noninteractive

echo -e "\e[32m Provisioning virtual machine... \e[0m"
apt-get update > /dev/null
apt-get -y upgrade > /dev/null

apt-get -y install mc > /dev/null
apt-get -y install wget > /dev/null

echo -e "\e[34m Устанавливаем Apache сервер \e[0m"
apt-get -y install apache2 > /dev/null

echo -e "\e[34m Устанавливаем PHP версии $PHPVERSION \e[0m"
apt-get -y install php$PHPVERSION libapache2-mod-php$PHPVERSION > /dev/null

echo -e "\e[34m Устанавливаем необходимые PHP расширения \e[0m"
apt-get -y install php$PHPVERSION-mysql > /dev/null
apt-get -y install php$PHPVERSION-memcached > /dev/null
apt-get -y install php$PHPVERSION-pgsql > /dev/null
apt-get -y install php$PHPVERSION-gd > /dev/null
apt-get -y install php$PHPVERSION-imagick > /dev/null
apt-get -y install php$PHPVERSION-intl > /dev/null
apt-get -y install php$PHPVERSION-xml > /dev/null
apt-get -y install php$PHPVERSION-zip > /dev/null
apt-get -y install php$PHPVERSION-mbstring > /dev/null
apt-get -y install php$PHPVERSION-curl > /dev/null

echo -e "\e[34m Удаляем все существующие виртуальные хосты \e[0m"
sudo rm -rf /etc/apache2/sites-available/*.conf
sudo rm -rf /etc/apache2/sites-enabled/*.conf

echo -e "\e[34m Добавляем виртуальные хосты из синхронизируемой папки \e[0m"
for vhFile in /var/tmp/skillbox-vhosts/*.conf
do
    # копируем конфигурационные файлы хостов в специально предназначенную для этого папку apache
    sudo cp /var/tmp/skillbox-vhosts/*.conf /etc/apache2/sites-available/ -R
    vhConf=${vhFile##*/}
    # регистрируем хосты
    sudo a2ensite ${vhConf}
    vhost=${vhConf%.*}
    # Добавляем запись в /etc/hosts
    sudo sed -i "2i${vmip}    ${vhost}" /etc/hosts
done

sudo service apache2 restart

apt-get -y install debconf-utils > /dev/null
# apt-get -y install vim > /dev/null
apt-get -y install curl > /dev/null
apt-get -y install build-essential > /dev/null
apt-get -y install software-properties-common > /dev/null
# apt-get -y install git > /dev/null

echo -e "\e[34m Устанавливаем MySQL root user password \e[0m"
debconf-set-selections <<< "mysql-server mysql-server/root_password password $DBPASSWD"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $DBPASSWD"
debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password $DBPASSWD"
debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password $DBPASSWD"
debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password $DBPASSWD"
debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect none"

echo -e "\e[34m Устанавливаем MySQL server \e[0m"
apt-get -y install mysql-server > /dev/null

echo -e "\e[34m Создаём базу данных $WPDBNAME для WordPress\e[0m"
mysql -uroot -p$DBPASSWD -e "CREATE DATABASE $WPDBNAME"
mysql -uroot -p$DBPASSWD -e "grant all privileges on $WPDBNAME.* to '$DBUSER'@'%' identified by '$DBPASSWD'"

echo -e "\e[34m Создаём базу данных $DPDBNAME для Drupal\e[0m"
mysql -uroot -p$DBPASSWD -e "CREATE DATABASE $DPDBNAME"
mysql -uroot -p$DBPASSWD -e "grant all privileges on $DPDBNAME.* to '$DBUSER'@'%' identified by '$DBPASSWD'"

echo -e "\e[34m Устанавливаем MySQL интерфейс phpMyAdmin \e[0m"
apt-get -y install phpmyadmin > /dev/null

sudo sed -i "s/.*bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/mysql.conf.d/mysqld.cnf
sudo service mysql restart
sudo a2enmod rewrite

sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/apache2.conf
sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php/$PHPVERSION/apache2/php.ini
sed -i "s/display_errors = .*/display_errors = On/" /etc/php/$PHPVERSION/apache2/php.ini

sudo service apache2 restart

wget https://ru.wordpress.org/latest-ru_RU.tar.gz -O ./wp.tar.gz -q
tar -xf ./wp.tar.gz -C /var/www
mv /var/www/wordpress/* /var/www/wordpress.local
rm ./wp.tar.gz
rm -Rf /var/www/wordpress
cp  /var/www/wp-config.php /var/www/wordpress.local/
wget https://api.wordpress.org/secret-key/1.1/salt/ -q -O - >> /var/www/wordpress.local/wp-config.php


wget https://www.drupal.org/download-latest/tar.gz -O ./drupal.tar.gz -q
tar -xf ./drupal.tar.gz -C /var/www
mv /var/www/drupal-*/* /var/www/drupal.local
rm ./drupal.tar.gz
rm -Rf /var/www/drupal-*
cp -Rf /var/www/drupal.settings.php /var/www/drupal.local/sites/default/
php -r "echo 'return ' . bin2hex(openssl_random_pseudo_bytes(10)) . ';';" > /var/www/drupal.local/sites/default/hash_salt.php
      
sudo service apache2 restart
